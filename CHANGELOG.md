# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2019-09-24

Gradient Textures

## [1.0.0] - 2019-09-04

Some code formating and changed version to 1.0 since package didn't change for some time and to get rid of the PREVIEW label.

## [0.0.1-preview.1] - 2018-07-26

This is the first release.
