﻿using System;
using Unity.Collections;
using UnityEngine;

namespace Monoid.Unity.Texture {

  public static class Gradients {

    public static bool IsOpaque(Gradient gradient) {
      foreach(var key in gradient.alphaKeys) {
        if (key.alpha < 1.0f) {
          return false;
        }
      }
      return true;
    }

    #region Managed Arrays

    public static void Sample(Gradient gradient, Color32[] samples) => Sample(gradient, samples, samples.Length);

    public static void Sample(Gradient gradient, Color32[] samples, int count, int start = 0) {
      for (int i = 0, n = count - 1; i <= n; ++i) {
        var t = i / (float)n;
        samples[start + i] = gradient.Evaluate(t);
      }
    }


    public static void Sample(Gradient gradient, Color[] samples) => Sample(gradient, samples, samples.Length);

    public static void Sample(Gradient gradient, Color[] samples, int count, int start = 0) {
      for (int i = 0, n = count - 1; i <= n; ++i) {
        var t = i / (float)n;
        samples[start + i] = gradient.Evaluate(t);
      }
    }

    #endregion

    #region Native Arrays

    public static void Sample(Gradient gradient, NativeArray<Color32> samples) => Sample(gradient, samples.Slice());

    public static void Sample(Gradient gradient, NativeSlice<Color32> samples) {
      for (int i = 0, n = samples.Length - 1; i <= n; ++i) {
        var t = i / (float)n;
        samples[i] = gradient.Evaluate(t);
      }
    }

    public static void Sample(Gradient gradient, NativeArray<Color> samples) => Sample(gradient, samples.Slice());

    public static void Sample(Gradient gradient, NativeSlice<Color> samples) {
      for (int i = 0, n = samples.Length - 1; i <= n; ++i) {
        var t = i / (float)n;
        samples[i] = gradient.Evaluate(t);
      }
    }

    #endregion

    #region Textures

    public static void Sample(Gradient gradient, Texture2D texture, bool updateMipmaps = true, bool makeNoLongerReadable = false) {
      int w = texture.width, h = texture.height;
      var colors = new Color[w*h];
      Sample(gradient, colors, w, 0);
      for (int y = 1; y < h; ++y) {
        Array.Copy(colors, 0, colors, y*w, w);
      }
      texture.SetPixels(colors);
      texture.Apply(updateMipmaps, makeNoLongerReadable);
    }

    public static void Sample(Gradient bottom, Gradient top, Texture2D texture, bool updateMipmaps = true, bool makeNoLongerReadable = false) {
      int w = texture.width, h = texture.height;
      var colors = new Color[w * h];
      Sample(bottom, colors, w);
      if (h > 1) {
        Sample(top, colors, w, h - 1);
        for (int y = 1, n = h - 1; y < n; ++y) {
          var alpha = y / (float)n;
          for (int x = 0; x < w; ++x) {
            colors[w * y + x] = Color.LerpUnclamped(colors[x], colors[w + n + x], alpha);
          }
        }
      }
      else {
        var tmp = new Color[w * h];
        Sample(top, tmp, w);
        for (int x = 0; x < w; ++x) {
          colors[x] = Color.LerpUnclamped(colors[x], tmp[x], 0.5f);
        }
      }
      texture.SetPixels(colors);
      texture.Apply(updateMipmaps, makeNoLongerReadable);
    }


    #endregion

    #region Gradient Evaluation (experimental)

    struct LerpKey {
      public static readonly LerpKey Initial = new LerpKey { from = 0, to = 1, alpha = 0.0f };

      public int from, to;
      public float alpha;
    }

    static Color GradientColor(ref LerpKey key, in NativeSlice<GradientColorKey> gradients, float time) {
      key.to = key.from + 1;
      while (key.to < gradients.Length && gradients[key.to].time < time) {
        key.from = key.to++;
      }
      key.alpha = (time - gradients[key.from].time) / (gradients[key.to].time - gradients[key.from].time);
      return Color.LerpUnclamped(gradients[key.from].color, gradients[key.to].color, key.alpha);
    }

    static float GradientAlpha(ref LerpKey key, in NativeSlice<GradientAlphaKey> gradients, float time) {
      key.to = key.from + 1;
      while (key.to < gradients.Length && gradients[key.to].time < time) {
        key.from = key.to++;
      }
      key.alpha = (time - gradients[key.from].time) / (gradients[key.to].time - gradients[key.from].time);
      return (1 - key.alpha) * gradients[key.from].alpha + gradients[key.to].alpha * key.alpha;
    }

    #endregion

  }

}