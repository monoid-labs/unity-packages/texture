TGA Image Conversion Package for Unity3D
========================================

Eoncides a _Texture2D_ into a byte sequence defined by the TGA/TARGA image format.



References
----------

- [Creating TGA Image files - Paul Bourke](http://www.paulbourke.net/dataformats/tga/)



License
-------

MIT License (see [LICENSE](LICENSE.md))
