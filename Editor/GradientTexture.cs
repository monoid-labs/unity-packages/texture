using System.IO;
using UnityEngine;
using UnityEditor.Experimental.AssetImporters;


namespace Monoid.Unity.Texture {

  [ScriptedImporter(1, "gradtex")]
  public class GradientTexture : ScriptedImporter {

    public Vector2Int size = new Vector2Int(16, 1);
    public Gradient gradient = new Gradient();
    public bool alphaIsTransparency = false;
    public TextureWrapMode wrapMode = TextureWrapMode.Clamp;
    public FilterMode filterMode = FilterMode.Bilinear;
    public bool generateMipMaps = false;
    public bool compress = false;
    public bool readWriteEnabled = true;

    public override void OnImportAsset(AssetImportContext ctx) {
      var path = ctx.assetPath;
      var name = Path.GetFileName(path);

      compress &= (size.x % 4 == 0) && (size.y % 4 == 0);

      var format = Gradients.IsOpaque(gradient) ? TextureFormat.RGB24 : TextureFormat.RGBA32;
      var texture = new Texture2D(size.x, size.y, format, generateMipMaps);
      texture.alphaIsTransparency = alphaIsTransparency;
      Gradients.Sample(gradient, texture, generateMipMaps);
      if (compress) {
        texture.Compress(true);
      }
      texture.filterMode = filterMode;
      texture.wrapMode = wrapMode;
      if (!readWriteEnabled) {
        texture.Apply(false, true);
      }

      ctx.AddObjectToAsset(name, texture);
      ctx.SetMainObject(texture);
    }
  }

}
